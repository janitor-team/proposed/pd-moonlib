pd-moonlib (0.4-1) unstable; urgency=medium

  * New upstream version 0.4
  * Declared that "root" is unneeded for building
  * Bumped standards version to 4.2.1

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Fri, 14 Sep 2018 21:18:47 +0200

pd-moonlib (0.3.6-3) unstable; urgency=medium

  * Switched to system-provided Makefile.pdlibbuilder (Closes: #889645)
    * Pass build-flags to all 'make' invocations

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 05 Feb 2018 14:42:53 +0100

pd-moonlib (0.3.6-2) unstable; urgency=medium

  * Simplified & unified d/rules
    * Bumped dh compat to 11
    * Enabled hardening
  * Updated Vcs-* stanzas to salsa.d.o
  * Updated maintainer address
  * Switched URLs to https://
  * Updated d/copyright(_hints)
  * Removed trailing whitespace in debian/*
  * Bumped standards version to 4.1.3

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 01 Feb 2018 23:23:45 +0100

pd-moonlib (0.3.6-1) unstable; urgency=medium

  * New upstream version 0.3.6
    (Closes: #792724)

  * Dropped unused patches
  * Updated d/copyright
  * Bumped standards to 4.0.1

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 16 Aug 2017 15:39:29 +0200

pd-moonlib (0.3.4~repack-2) unstable; urgency=medium

  * Use system-provided Makefile.pd-lib-builder
  * Stripped externals
  * Fixed typos in package description
  * Adjusted path in lintian-override

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 05 Jan 2017 20:12:30 +0100

pd-moonlib (0.3.4~repack-1) unstable; urgency=medium

  * New upstream version 0.3.4~repack

  [ Hans-Christoph Steiner ]
  * Updated to copyright-format/1.0
  * Updated Depends: pd to use puredata 0.43 style
  * Removed 'DM-Upload-Allowed: yes', its deprecated

  [ IOhannes m zmölnig ]
  * Added myself to uploaders
  * Updated Vcs-* stanzas to https://
  * Demoted pd-libdir to Recommends
  * Filtered-out '.git*' and '*.dll' when importing
  * Removed obsolete patches
  * Updated homepage field (github)
  * Updated d/watch
  * Updated d/copyright
  * Dropped arch-specific optimisation and pass hardening flags
  * Bumped compat to 9 (for hardening flags)
  * Bumped standards-version to 3.9.8

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Fri, 28 Oct 2016 23:42:39 +0200

pd-moonlib (0.2-2) unstable; urgency=low

  * Team upload

  [ Hans-Christoph Steiner ]
  * documented the LGPL-2.1 files
  * added DM-Upload-Allowed: yes since I'm now a DM
  * updated Build-Depends to use puredata-dev when available
  * bumped standards version to 3.9.2
  * added Vcs-* stanzas

  [ Felipe Sateler ]
  * Drop quilt since we use the 3.0 source format
  * Depend on real package puredata as default alternative

 -- Felipe Sateler <fsateler@debian.org>  Wed, 22 Jun 2011 19:07:59 -0400

pd-moonlib (0.2-1) unstable; urgency=low

  * Initial release (Closes: #591737)

 -- Hans-Christoph Steiner <hans@eds.org>  Wed, 10 Nov 2010 15:36:13 -0500
